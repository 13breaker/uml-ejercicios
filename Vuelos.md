#Vuelos


``` mermaid
classDiagram

Aviones <-- Vuelos
Vuelos <-- Billetes
Billetes -- Persona

class Vuelos{
    -Fechas: Date
    -Numero plazas: Integer
    +Ofertan vuelos: () void
}
class Aviones{
    -Capacidad: Integer
    +Disponibilidad asientos: () void
}
class Persona{
    -Nombre: String
    -Apellidos: String
    -Edad: Integer
    +Compran: () void
    
}
class Billetes{
    -Asiento: Integer
}