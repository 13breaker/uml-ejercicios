#Biblioteca


``` mermaid
classDiagram

Prestamo .. Socio
Libros -- Copia
Libros -- Autor
Socio -- Copia
Socio -- Multa

class Libros {
    -Nombre: String
    -Editorial: String
    -Año: Integer
    -Tipo: Genero
}
class Autor{
    -Nombre: String
    -Nacionalidad_: String
    -Fecha nacimiento: Date
}
class Copia {
    -Referencia: Integer
    -Estado: EstadoCopia
}
class Socio{
    -Numero: Integer
    -Nombre: String
    -Direccion: String
    -Telefono: String
}
class Prestamo{
    -Inicio: Date
    -Fin: Date
}
class Multa{
    -Inicio: Date
    -Fin: Date
}
class Genero{
    -Novela
    -Teatro
    -Poesia
    -Ensayo
}
class EstadoCopia{
    -Prestado
    -Retraso
    -En biblioteca
    -Reparacion
}