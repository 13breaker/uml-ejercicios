#Proyectos


``` mermaid
classDiagram

Proyecto o-- Ciclos
Ciclos <-- Fase Inicio
Ciclos <-- Fase Elaboracion
Ciclos <-- Fase Transicion
Ciclos <-- Fase Construccion
Fase Inicio <-- Iteraciones
Fase Elaboracion <-- Iteraciones
Fase Transicion <-- Iteraciones
Fase Construccion <-- Iteraciones
Iteraciones <|-- Artefactos


class Proyecto{
    -Nombre: String
}
class Ciclos{
    -Version ejecutable: String
}
class Fase Inicio{
   -Fase superada
   +S/N: () void
}
class Fase Elaboracion{
   -Fase superada
   +S/N: () void
}
class Fase Transicion{
   -Fase superada
   +S/N: () void
}
class Fase Construccion{
   -Fase superada
   +S/N: () void
}
class Iteraciones{
    -Actividades: String
    -Duracion: Date
    -Recursos: Materiales y humanos
    +Artefactos: () void
}
class Artefactos{
    -Documentacion
    -Resultados de prueba
    -Software
}
class Medicion Proyecto{
    -Avance del proyecto: String
}