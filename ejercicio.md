#Proyectos


``` mermaid
classDiagram

<!--
En este diagrama habran empleados con sus respectivos atributos como nº empleado y fecha de incorporacion. 
Los cuales se les filtrara por su sueldo, se les aplicara un porcentaje de impuestos en relacion con su sueldo. Si superan los 1750€ y tienen una antigüedad superior a 3 años serán candidatos a ascenso según sus mérito
Ademas pueden pertenecer a distintos departamentos ocmo informatica, seguridad, administracion o RRHH-->

Empleados -- Personas
Impuestos *-- Empleados
Departamentos <|-- Empleados
Empleados o-- Candidatos
Impuestos *-- Candidatos

class Empleados{
    -Nº empleado: Integer
    -Fecha incorporacion: Date
    +Supera [S/N]: () void
}
class Personas{
    -Nombre: String
    -Apellidos: String
    -Edad: Integer
}
class Impuestos{
    -Hasta 1100€ = 10%: Integer
    -Desde 1100€ = 15%: Integer
    -Desde 1500€ = 25%: Integer
    +Nº empleados[>=1500€]: () void
}
class Departamentos{
    -Informatica
    -Seguridad
    -Administracion
    -RRHH
    +Nº empleados: () void
}
class Candidatos{
    -Antigüedad 3 años
    -No superar 1750€
    +Mostrar candidatos: () void
}